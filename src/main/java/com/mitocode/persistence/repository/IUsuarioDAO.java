package com.mitocode.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.persistence.model.Usuario;

public interface IUsuarioDAO extends JpaRepository<Usuario, Long> {
	
	Usuario findOneByUsername(String username);
}