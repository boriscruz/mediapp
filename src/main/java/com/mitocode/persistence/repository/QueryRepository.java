package com.mitocode.persistence.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.mitocode.persistence.model.Query;

public interface QueryRepository extends JpaRepository<Query, Integer> {

	@org.springframework.data.jpa.repository.Query("from Query c where c.patient.dni =:dni or LOWER(c.patient.name) like %:nombreCompleto% or LOWER(c.patient.lastname) like %:nombreCompleto%")
	List<Query> buscar(@Param("dni") String dni, @Param("nombreCompleto") String nombreCompleto);

	@org.springframework.data.jpa.repository.Query("from Query c where c.date between :fechaConsulta and :fechaSgte")
	List<Query> buscarPorFecha(@Param("fechaConsulta") LocalDateTime fechaConsulta,
			@Param("fechaSgte") LocalDateTime fechaSgte);

	@org.springframework.data.jpa.repository.Query(value = "select * from fn_listarResumen()", nativeQuery= true)
	List<Object[]> listarResumen();
}
