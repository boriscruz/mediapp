package com.mitocode.persistence.repository;

import com.mitocode.persistence.model.Doctor;

public interface DoctorRepository extends PersonRepository<Doctor> {

}
