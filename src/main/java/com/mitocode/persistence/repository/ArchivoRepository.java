package com.mitocode.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.persistence.model.Archivo;

public interface ArchivoRepository extends JpaRepository<Archivo, Integer> {

}
