package com.mitocode.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.persistence.model.Person;

public interface PersonRepository<T extends Person> extends JpaRepository<T, Long> {

}
