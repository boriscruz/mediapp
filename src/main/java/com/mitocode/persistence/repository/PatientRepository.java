package com.mitocode.persistence.repository;

import com.mitocode.persistence.model.Patient;

public interface PatientRepository extends PersonRepository<Patient> {

}
