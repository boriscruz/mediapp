package com.mitocode.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.persistence.model.DetailQuery;

public interface DetailQueryRepository extends JpaRepository<DetailQuery, Integer> {

}
