package com.mitocode.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.persistence.model.Examination;

public interface ExaminationRepository extends JpaRepository<Examination, Integer> {

}
