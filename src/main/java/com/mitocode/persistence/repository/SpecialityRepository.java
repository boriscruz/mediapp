package com.mitocode.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.persistence.model.Speciality;

public interface SpecialityRepository extends JpaRepository<Speciality, Integer> {

}
