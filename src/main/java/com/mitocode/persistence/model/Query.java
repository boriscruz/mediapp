/**
 * @author boris
 */
package com.mitocode.persistence.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Información de una consulta")
@Entity
@Table(name = "query")
@Data
public class Query {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idQuery;

	/**
	 * Fecha que se realiza la consulta
	 */
	@ApiModelProperty(notes = "Fecha que se realiza la consulta")
	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime date;

	/**
	 * Paciente que tiene la consulta
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "id_patient", nullable = false)
	private Patient patient;

	/**
	 * Doctor que atiende la consulta
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "id_doctor", nullable = false)
	private Doctor doctor;

	/**
	 * Especialidad que trata la consulta
	 */
	@ManyToOne
	@NotNull
	@JoinColumn(name = "id_speciality", nullable = false)
	private Speciality speciality;

	/**
	 * Detalle de la consulta
	 */
	@OneToMany(mappedBy = "idQuery", cascade = { CascadeType.PERSIST, CascadeType.MERGE,
			CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<DetailQuery> detailQuery;

	/**
	 * Tabla que relaciona los consultas con los examenes
	 */
	@ManyToMany(cascade = { CascadeType.REFRESH })
	@JoinTable(name = "query_examination", joinColumns = @JoinColumn(name = "id_query", foreignKey = @ForeignKey(name = "query_examination_fkey")), inverseJoinColumns = @JoinColumn(name = "id_examination", foreignKey = @ForeignKey(name = "examination_query_fkey")))
	private List<Examination> examList;

	public Query() {
	}

}
