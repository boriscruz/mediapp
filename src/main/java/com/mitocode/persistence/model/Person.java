/**
 * @author boris
 */
package com.mitocode.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Información personal de una persona")
@Entity
@Table(name = "person")
@Data
@Inheritance(strategy = InheritanceType.JOINED)
public class Person {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * Nombre completo
	 */
	@ApiModelProperty(notes = "Debe tener como mínimo 3 caracteres y máximo 70 caracteres")
	@Size(min = 3, max=70, message = "Debe tener como mínimo 3 caracteres")
	@Column(name = "name", nullable = true, length = 70)
	private String name;

	/**
	 * Apellido completo
	 */
	@ApiModelProperty(notes = "Debe tener como mínimo tres caracteres y máximo 70 caracteres")
	@Size(min = 3, max=70, message = "Debe tener como mínimo tres caracteres y máximo 70 caracteres")
	@Column(name = "lastname", nullable = true, length = 70)
	private String lastname;

	/**
	 * Número de DNI
	 */
	@ApiModelProperty(notes = "El DNI debe tener 8 caracteres")
	@Size(min = 8, max = 8, message = "El DNI debe tener 8 caracteres")
	@Column(name = "dni", nullable = false, length = 8, unique = true)
	private String dni;

	/**
	 * Dirección de residencia
	 */
	@ApiModelProperty(notes = "La dirección debe tener como mínimo 3 caracteres")
	@Size(min = 3, max = 150, message = "La dirección debe tener como mínimo 3 caracteres y máximo 150 caracteres")
	@Column(name = "address", length = 150)
	private String address;

	/**
	 * Número de teléfono celular
	 */
	@ApiModelProperty(notes = "Teléfono debe tener 10 caracteres")
	@Size(min = 10, max = 10, message = "Teléfono debe tener 10 caracteres")
	@Column(name = "phone", length = 10)
	private String phone;
	
	public Person() {
	}

}
