/**
 * @author boris
 */
package com.mitocode.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Información del detalle de una consulta")
@Entity
@Table(name = "detail_query")
@Data
public class DetailQuery {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ApiModelProperty(notes = "El diagnóstivo no puede ser superior a 70 caracteres")
	@Size(max = 70, message = "El diagnóstivo no puede ser superior a 70 caracteres")
	@Column(name = "diagnosis", nullable = false, length = 70)
	private String diagnosis;

	@ApiModelProperty(notes = "El diagnóstivo no puede ser superior a 300 caracteres")
	@Size(max = 300, message = "El diagnóstivo no puede ser superior a 300 caracteres")
	@Column(name = "treatment", nullable = false, length = 300)
	private String treatment;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "id_query", nullable = false)
	private Query idQuery;

	public DetailQuery() {
	}

}
