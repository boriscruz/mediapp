/**
 * @author boris
 */
package com.mitocode.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(description = "Información personal de una doctor")
@Entity
@Table(name = "doctor")
@EqualsAndHashCode(callSuper = false)
@Data
public class Doctor extends Person {

	/**
	 * Número de matricula
	 */
	@ApiModelProperty(notes = "El cmp debe tener como máximo 70 caracteres")
	@Size(max=70, message="El cmp debe tener como máximo 70 caracteres")
	@Column(name = "cmp", length = 70, nullable = false)
	private String cmp;

	public Doctor() {
		super();
	}

}
