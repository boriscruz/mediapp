/**
 * @author boris
 */
package com.mitocode.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Información sobre la especialidad de la consulta")
@Entity
@Table(name = "speciality")
@Data
public class Speciality {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idSpeciality;

	/**
	 * Nombre de la especialidad
	 */
	@ApiModelProperty(notes = "El nombre no puede tener más de 70 caracteres")
	@NotNull
	@Size(max = 70, message = "El nombre no puede tener más de 70 caracteres")
	@Column(name = "name", nullable = false, length = 70)
	private String name;

	public Speciality() {
	}

}
