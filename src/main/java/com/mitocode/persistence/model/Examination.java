/**
 * @author boris
 */
package com.mitocode.persistence.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Exámenes que se realiza un paciente")
@Entity
@Table(name = "examination")
@Data
public class Examination {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	/**
	 * Nombre del examen
	 */
	@ApiModelProperty(notes = "El nombre del examen no puede tener más de 70 caracteres")
	@NotNull
	@Size(max = 70, message = "El nombre del examen no puede tener más de 70 caracteres")
	@Column(name = "name", nullable = false, length = 70)
	private String name;

	/**
	 * Breve descripción del examen
	 */
	@ApiModelProperty(notes = "La descripción no puede tener más de 150 caracteres")
	@Size(max = 150, message = "La descripción no puede tener más de 150 caracteres")
	@Column(name = "description", length = 150)
	private String description;

	@JsonIgnore
	@ManyToMany(mappedBy = "examList")
	private List<Query> queryList;

	public Examination() {
	}

}
