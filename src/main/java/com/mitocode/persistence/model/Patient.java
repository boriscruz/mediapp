/**
 * @author boris
 */
package com.mitocode.persistence.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@ApiModel(description = "Información personal de una paciente")
@Entity
@Table(name = "patient")
@Data
@EqualsAndHashCode(callSuper=false)
public class Patient extends Person{

	public Patient() {
		super();
	}

}
