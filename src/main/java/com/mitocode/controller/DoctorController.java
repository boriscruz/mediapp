/**
 * @author boris
 */
package com.mitocode.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.exception.ModelNotFoundException;
import com.mitocode.persistence.model.Doctor;
import com.mitocode.service.DoctorService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/doctors")
public class DoctorController {

	private final Logger log = LoggerFactory.getLogger(DoctorController.class);

	private static final String mediaType = MediaType.APPLICATION_JSON_UTF8_VALUE;

	@Autowired
	private DoctorService service;

	/**
	 * Obtiene todos los elementos
	 * 
	 * @return
	 */
	@ApiOperation(value = "Obtiene la lista completa de todos los elementos")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "La lista de elementos se obtuvo correctamente"),
			@ApiResponse(code = 204, message = "No hay ningún elemento almacenado", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@GetMapping(produces = mediaType)
	public ResponseEntity<List<Doctor>> findAll() {
		log.debug("Getting all doctors");
		final List<Doctor> doctors = service.findAll();
		final HttpStatus status = doctors.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK;
		return new ResponseEntity<List<Doctor>>(doctors, status);
	}

	/**
	 * Se busca a un elemento en particular mediante su clave
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Obtiene un elemento dado su identificador")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "El elemento fue encontrado correctamente"),
			@ApiResponse(code = 404, message = "No se pudo encontrar el elemento", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@GetMapping(value = "/{id}", produces = mediaType)
	public ResponseEntity<Doctor> findOne(@PathVariable("id") Long id) {
		log.debug("getting a doctor with id: ", id);
		final Doctor d = service.findOne(id)
				.orElseThrow(() -> new ModelNotFoundException("No se encontró el elemento con el id " + id));
		return new ResponseEntity<Doctor>(d, HttpStatus.OK);
	}

	/**
	 * Registra un nuevo elemento en el sistema
	 * 
	 * @param doctor
	 * @return
	 */
	@ApiOperation(value = "Registra un nuevo elemento en el sistema")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "El elemento fue registrado correctamente"),
			@ApiResponse(code = 400, message = "El elemento no cumple con las validaciones correspondientes", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@PostMapping(consumes = mediaType, produces = mediaType)
	public ResponseEntity<Doctor> save(@Valid @RequestBody Doctor doctor) {
		log.debug("Saving a doctor {}", doctor);
		Doctor d = service.save(doctor);
		return new ResponseEntity<Doctor>(d, HttpStatus.CREATED);
	}

	/**
	 * Se modifica un elemento
	 * 
	 * @param doctor
	 * @return
	 */
	@ApiOperation(value = "Actualiza un nuevo elemento en el sistema")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "El elemento fue modificado correctamente"),
			@ApiResponse(code = 400, message = "El elemento no cumple con las validaciones correspondientes", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@PutMapping(consumes = mediaType)
	public ResponseEntity<Doctor> update(@Valid @RequestBody Doctor doctor) {
		log.debug("Modifying doctor {}", doctor);
		Doctor d = service.save(doctor);
		return new ResponseEntity<Doctor>(d, HttpStatus.CREATED);
	}

	/**
	 * Elimina un elemento del sistema
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Elimina el elemento del sistema")
	@ApiResponses({ @ApiResponse(code = 200, message = "El elemento fue eliminado correctamente"),
			@ApiResponse(code = 404, message = "El elemento no existe", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@DeleteMapping(value = "/{id}", produces = mediaType)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("Deleting a doctor with id: ", id);
		service.findOne(id).orElseThrow(() -> new ModelNotFoundException("No se encontró el elemento con el id " + id));
		service.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
