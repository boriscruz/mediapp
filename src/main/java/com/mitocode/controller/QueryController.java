/**
 * @author boris
 */
package com.mitocode.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.exception.ModelNotFoundException;
import com.mitocode.persistence.model.Archivo;
import com.mitocode.persistence.model.Query;
import com.mitocode.service.ArchivoService;
import com.mitocode.service.QueryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/queries")
public class QueryController {

	private final Logger log = LoggerFactory.getLogger(DoctorController.class);

	private static final String mediaType = MediaType.APPLICATION_JSON_UTF8_VALUE;

	@Autowired
	private QueryService service;
	
	@Autowired
	private ArchivoService fileService;

	/**
	 * Obtiene todos los elementos
	 * 
	 * @return
	 */
	@ApiOperation(value = "Obtiene la lista completa de todos los elementos")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "La lista de elementos se obtuvo correctamente"),
			@ApiResponse(code = 204, message = "No hay ningún elemento almacenado", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@GetMapping(produces = mediaType)
	public ResponseEntity<List<Query>> findAll() {
		log.debug("Getting all queries");
		final List<Query> queries = service.findAll();
		final HttpStatus status = queries.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK;
		return new ResponseEntity<List<Query>>(queries, status);
	}

	/**
	 * Se busca a un elemento en particular mediante su clave
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Obtiene un elemento dado su identificador")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "El elemento fue encontrado correctamente"),
			@ApiResponse(code = 404, message = "No se pudo encontrar el elemento", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@GetMapping(value = "/{id}", produces = mediaType)
	public ResponseEntity<Query> findOne(@PathVariable("id") Integer id) {
		log.debug("getting a query with id: ", id);
		final Query q = service.findOne(id)
				.orElseThrow(() -> new ModelNotFoundException("No se encontró el elemento con el id " + id));
		return new ResponseEntity<Query>(q, HttpStatus.OK);
	}

	/**
	 * Registra un nuevo elemento en el sistema
	 * 
	 * @param query
	 * @return
	 */
	@ApiOperation(value = "Registra un nuevo elemento en el sistema")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "El elemento fue registrado correctamente"),
			@ApiResponse(code = 400, message = "El elemento no cumple con las validaciones correspondientes", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@PostMapping(consumes = mediaType, produces = mediaType)
	public ResponseEntity<Query> save(@Valid @RequestBody Query query) {
		log.debug("Saving a query {}", query);
		Query q = service.save(query);
		return new ResponseEntity<Query>(q, HttpStatus.CREATED);
	}

	/**
	 * Se modifica un elemento
	 * 
	 * @param query
	 * @return
	 */
	@ApiOperation(value = "Actualiza un nuevo elemento en el sistema")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "El elemento fue modificado correctamente"),
			@ApiResponse(code = 400, message = "El elemento no cumple con las validaciones correspondientes", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@PutMapping(consumes = mediaType)
	public ResponseEntity<Query> update(@Valid @RequestBody Query query) {
		log.debug("Saving a query {}", query);
		Query q = service.save(query);
		return new ResponseEntity<Query>(q, HttpStatus.CREATED);
	}

	/**
	 * Elimina un elemento del sistema
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Elimina el elemento del sistema")
	@ApiResponses({ @ApiResponse(code = 200, message = "El elemento fue eliminado correctamente"),
			@ApiResponse(code = 404, message = "El elemento no existe", response = void.class),
			@ApiResponse(code = 500, message = "Se ha producido un error inesperado", response = void.class) })
	@DeleteMapping(value = "/{id}", produces = mediaType)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		log.debug("Deleting a query with id: ", id);
		service.findOne(id).orElseThrow(() -> new ModelNotFoundException("No se encontró el elemento con el id " + id));
		service.delete(id);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@PostMapping(value = "/buscar", produces = mediaType, consumes = mediaType)
	public ResponseEntity<List<Query>> buscar(@RequestBody FiltroConsultaDTO filtro) {
		List<Query> consultas = new ArrayList<>();

		if (filtro != null) {
			if (filtro.getFechaConsulta() != null) {
				consultas = service.buscarfecha(filtro);
			} else {
				consultas = service.buscar(filtro);
			}
		}

		return new ResponseEntity<List<Query>>(consultas, HttpStatus.OK);
	}
	
	@GetMapping(value = "/listarResumen", produces = mediaType)
	public ResponseEntity<List<ConsultaResumenDTO>> listarResumen() {
		List<ConsultaResumenDTO> consultas = new ArrayList<>();
		consultas = service.listarResumen();
		return new ResponseEntity<List<ConsultaResumenDTO>>(consultas, HttpStatus.OK);
	}
	
	@GetMapping(value = "/generarReporte", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> generarReporte() {
		byte[] data = null;
		data = service.generarReporte();
		return new ResponseEntity<byte[]>(data, HttpStatus.OK);
	}
	
	@PostMapping(value = "/guardarArchivo")
	public ResponseEntity<Integer> guardarArchivo(@RequestParam("file") MultipartFile file) throws IOException {
		
		int rpta = 0;
		Archivo ar = new Archivo();
		ar.setValue(file.getBytes());
		rpta = fileService.guardar(ar); 				

		return new ResponseEntity<Integer>(rpta, HttpStatus.OK);
	}
	
	@GetMapping(value = "/leerArchivo/{idArchivo}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<byte[]> leerArchivo(@PathVariable("idArchivo") Integer idArchivo) throws IOException {
				
		byte[] arr = fileService.leerArchivo(idArchivo); 

		return new ResponseEntity<byte[]>(arr, HttpStatus.OK);
	}

}
