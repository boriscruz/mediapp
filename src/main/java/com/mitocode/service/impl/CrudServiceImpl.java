package com.mitocode.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.service.CrudService;

public abstract class CrudServiceImpl<T, K extends Serializable> implements CrudService<T, K> {

	/**
	 * Se accede al repositorio de cada clase
	 * 
	 * @return
	 */
	public abstract JpaRepository<T, K> getRepository();

	@Override
	public T save(T entity) {
		return getRepository().save(entity);
	}

	@Override
	public void delete(T entity) {
		getRepository().delete(entity);
	}

	@Override
	public void delete(K id) {
		getRepository().deleteById(id);
	}

	@Override
	public Optional<T> findOne(K id) {
		return getRepository().findById(id);
	}

	@Override
	public List<T> findAll() {
		return getRepository().findAll();
	}

}
