package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.persistence.model.Doctor;
import com.mitocode.persistence.repository.DoctorRepository;
import com.mitocode.service.DoctorService;

@Service
public class DoctorServiceImpl extends CrudServiceImpl<Doctor, Long> implements DoctorService {

	@Autowired
	private DoctorRepository repository;
	
	@Override
	public JpaRepository<Doctor, Long> getRepository() {
		return repository;
	}

}
