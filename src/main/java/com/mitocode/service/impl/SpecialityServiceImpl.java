package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.persistence.model.Speciality;
import com.mitocode.persistence.repository.SpecialityRepository;
import com.mitocode.service.SpecialityService;

@Service
public class SpecialityServiceImpl extends CrudServiceImpl<Speciality, Integer> implements SpecialityService {

	@Autowired
	private SpecialityRepository repository;

	@Override
	public JpaRepository<Speciality, Integer> getRepository() {
		return repository;
	}

}
