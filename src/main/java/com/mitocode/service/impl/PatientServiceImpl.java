package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.persistence.model.Patient;
import com.mitocode.persistence.repository.PatientRepository;
import com.mitocode.service.PatientService;

@Service
public class PatientServiceImpl extends CrudServiceImpl<Patient, Long> implements PatientService {

	@Autowired
	private PatientRepository repository;

	@Override
	public JpaRepository<Patient, Long> getRepository() {
		return repository;
	}

	@Override
	public Page<Patient> findAllPageable(Pageable pageable) {
		return repository.findAll(pageable);
	}

}
