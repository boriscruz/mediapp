package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.persistence.model.DetailQuery;
import com.mitocode.persistence.repository.DetailQueryRepository;
import com.mitocode.service.DetailQueryService;

@Service
public class DetailQueryServiceImpl extends CrudServiceImpl<DetailQuery, Integer> implements DetailQueryService {

	@Autowired
	private DetailQueryRepository repository;

	@Override
	public JpaRepository<DetailQuery, Integer> getRepository() {
		return repository;
	}

}
