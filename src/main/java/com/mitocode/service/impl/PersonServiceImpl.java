package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.persistence.model.Person;
import com.mitocode.persistence.repository.PersonRepository;

@Service
public class PersonServiceImpl extends CrudServiceImpl<Person, Long> {

	@Autowired
	@Qualifier("personRepository")
	private PersonRepository<Person> repository; 
	
	@Override
	public JpaRepository<Person, Long> getRepository() {
		return repository;
	}

}
