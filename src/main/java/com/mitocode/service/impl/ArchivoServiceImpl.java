package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.persistence.model.Archivo;
import com.mitocode.persistence.repository.ArchivoRepository;
import com.mitocode.service.ArchivoService;

@Service
public class ArchivoServiceImpl extends CrudServiceImpl<Archivo, Integer> implements ArchivoService {

	@Autowired
	private ArchivoRepository repository;
	
	@Override
	public JpaRepository<Archivo, Integer> getRepository() {
		return repository;
	}
	
	@Override
	public Integer guardar(Archivo archivo) {
		Archivo ar = repository.save(archivo);
		return ar.getIdArchivo() > 0 ? 1 : 0;
	}

	@Override
	public byte[] leerArchivo(Integer idArchivo) {
		return repository.getOne(idArchivo).getValue();
	}

	

}
