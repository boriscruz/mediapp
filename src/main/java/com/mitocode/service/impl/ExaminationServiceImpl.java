package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.mitocode.persistence.model.Examination;
import com.mitocode.persistence.repository.ExaminationRepository;
import com.mitocode.service.ExaminationService;

@Service
public class ExaminationServiceImpl extends CrudServiceImpl<Examination, Integer> implements ExaminationService {

	@Autowired
	private ExaminationRepository repository;

	@Override
	public JpaRepository<Examination, Integer> getRepository() {
		return repository;
	}

}
