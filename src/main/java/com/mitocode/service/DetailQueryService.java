package com.mitocode.service;

import com.mitocode.persistence.model.DetailQuery;

public interface DetailQueryService extends CrudService<DetailQuery, Integer> {

}
