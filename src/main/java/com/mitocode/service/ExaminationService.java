package com.mitocode.service;

import com.mitocode.persistence.model.Examination;

public interface ExaminationService extends CrudService<Examination, Integer> {

}
