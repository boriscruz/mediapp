package com.mitocode.service;

import java.util.List;

import com.mitocode.dto.ConsultaResumenDTO;
import com.mitocode.dto.FiltroConsultaDTO;
import com.mitocode.persistence.model.Query;

public interface QueryService extends CrudService<Query, Integer> {

	List<Query> buscar(FiltroConsultaDTO filtro);

	List<Query> buscarfecha(FiltroConsultaDTO filtro);
	
	List<ConsultaResumenDTO> listarResumen();
	
	byte[] generarReporte();
}
