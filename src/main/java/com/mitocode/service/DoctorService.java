package com.mitocode.service;

import com.mitocode.persistence.model.Doctor;

public interface DoctorService extends CrudService<Doctor, Long> {

}
