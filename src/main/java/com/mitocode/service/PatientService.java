package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.persistence.model.Patient;

public interface PatientService extends CrudService<Patient, Long> {
	
	Page<Patient> findAllPageable(Pageable pageable);

}
