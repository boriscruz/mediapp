package com.mitocode.service;

import java.util.List;

import com.mitocode.persistence.model.Menu;

public interface MenuService extends CrudService<Menu, Integer> {
	
	List<Menu> listarMenuPorUsuario(String nombre);

}
