package com.mitocode.service;

import com.mitocode.persistence.model.Speciality;

public interface SpecialityService extends CrudService<Speciality, Integer> {

}
