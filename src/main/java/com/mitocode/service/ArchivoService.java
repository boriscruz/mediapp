package com.mitocode.service;

import com.mitocode.persistence.model.Archivo;

public interface ArchivoService extends CrudService<Archivo, Integer> {

	Integer guardar(Archivo archivo);
	byte[] leerArchivo(Integer idArchivo);
}
