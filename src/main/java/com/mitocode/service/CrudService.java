package com.mitocode.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

public interface CrudService<T, K extends Serializable> {

	/**
	 * Agregar o editar entidad
	 * 
	 * @param entity
	 *            - Entidad a editar o agregar
	 * @return entity - Entidad agregada o modificada
	 */
	T save(T entity);

	/**
	 * Elimina una entidad
	 * 
	 * @param entity
	 *            - Entidad a ser eliminada
	 */
	void delete(T entity);
	
	/**
	 * Elimina una entidad mediante su clave
	 * @param id
	 */
	void delete(K id);

	/**
	 * Buscar una entidad en particular
	 * 
	 * @param id
	 *            - Clave de la entidad
	 * @return Optional<T> Entidad que se busca
	 */
	Optional<T> findOne(K id);

	/**
	 * Obtener todos los elementos de la entidad
	 * 
	 * @return
	 */
	List<T> findAll();

}
