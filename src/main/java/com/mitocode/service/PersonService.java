package com.mitocode.service;

import com.mitocode.persistence.model.Person;

public interface PersonService extends CrudService<Person, Long> {

}
