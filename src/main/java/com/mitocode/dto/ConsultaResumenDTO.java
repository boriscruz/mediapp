package com.mitocode.dto;

import lombok.Data;

@Data
public class ConsultaResumenDTO {

	private Integer cantidad;
	private String fecha;

	public ConsultaResumenDTO() {

	}

	public ConsultaResumenDTO(Integer cantidad, String fecha) {
		super();
		this.cantidad = cantidad;
		this.fecha = fecha;
	}

}