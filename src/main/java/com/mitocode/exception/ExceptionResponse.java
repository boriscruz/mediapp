/**
 * @author mitocode
 */
package com.mitocode.exception;

import java.util.Date;

import lombok.Data;

@Data
public class ExceptionResponse {

	/**
	 * Fecha y hora del error
	 */
	private Date timestamp;

	/**
	 * Mensaje del error
	 */
	private String mensaje;

	/**
	 * Detalles adicionales que puede tener el error
	 */
	private String detalles;

	public ExceptionResponse() {
	}

	public ExceptionResponse(Date timestamp, String mensaje, String detalles) {
		super();
		this.timestamp = timestamp;
		this.mensaje = mensaje;
		this.detalles = detalles;
	}

}
